# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=50000
SAVEHIST=50000

setopt append_history autocd extended_glob share_history correct nonomatch

bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# add typed commands to cmd history so they can be recalled with `cmd -<TAB>`
setopt autopushd pushd_ignore_dups

export TERM=screen-256color-bce

## case-insensitive (all),partial-word and then substring completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' \
    'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# rehash automatically so new files in $PATH are found for auto-completion
zstyle ':completion:*' rehash true

# Git-related information
# See <https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Zsh>
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:*' enable git bzr
zstyle ':vcs_info:git:*' formats '%b支'

# Prompt
# ------
# Left prompt shows current directory
# %~ means it prints ~ instead of whole path for $HOME dir
# %(!.#.$) means "use # if user with privileges, else $"
PROMPT='%~%(!.#.$) '

# Key binding
# -----------
# With this, typing the beginning of a command in history, then Up arrow will
# show the previously input commands starting with that.

autoload -Uz up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey '\eOA' up-line-or-beginning-search
bindkey '\e[A' up-line-or-beginning-search
bindkey '\eOB' down-line-or-beginning-search
bindkey '\e[B' down-line-or-beginning-search
# Replicate Bash's Ctrl-U behavior: https://stackoverflow.com/a/3483679/1844518
bindkey \^U backward-kill-line

# Byobu
#if which byobu >/dev/null 2>&1; then
    # if not inside a byobu session, and if no session is started,
    # start a new session
#    test -z "$TMUX" && (byobu attach || byobu new-session)
#fi

# aliases
alias ls="ls --color=auto"
alias ll="ls -lah"
alias xo="xdg-open"
alias ip="ip --color"
alias df="df -x squashfs"

# exports

# set PATH so it includes user's local bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# set PATH so it includes user's local bin if it exists
if [ -d "/snap/bin" ] ; then
    PATH="/snap/bin:$PATH"
fi

# source additional stuff from a personal zshrc if it exists
if [ -f "$HOME/.zshrc-$USER" ] ; then
    source "$HOME/.zshrc-$USER"
fi

# source z.sh if it exists
# see https://github.com/rupa/z
if [ -f "$HOME/.local/bin/z.sh" ] ; then
    source "$HOME/.local/bin/z.sh"
fi
